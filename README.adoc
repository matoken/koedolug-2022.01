= 2021-11-13(sat) 「link:https://koedolug.connpass.com/event/227471/[小江戸らぐ 11月のオフな集まり(第232回)」発表資料

== dir

.スライド
----
slide/
├── slide.adoc
├── slide.html
├── slide.pdf
└── slide.pdf.sig
----

.設定ファイル
----
slide/
└── resources
    ├── ksmbd.smb.conf
    └── samba.smb.conf
----

== url

* link:https://wiki.matoken.org/linux/cifs/ksmbd[linux:cifs:ksmbd [wiki.matoken.org]]
